﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_work.Entity
{
    public class PaymentTypeEntity
    {

        public int PaymentTypeID { get; set; }
        public string PaymentType { get; set; }
        public string Description { get; set; }
    }
}