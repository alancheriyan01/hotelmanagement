﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_work.Entity
{
    public class InvoiceEntity
    {
        public int Invoice_number { get; set; }

        public int Reservation_Id { get; set; }
        public string Payment_Date { get; set; }
        public int Payment_Id { get; set; }
        public double Total_amount { get; set; }
        public string Status { get; set; }

    }
}
