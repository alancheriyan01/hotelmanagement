﻿
namespace project_work.Entity
{
    public class CustomerEntity
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail{get; set; }
        public string Password { get; set;  }
        public string CustomerCompany { get; set; }
        public string CustomerDiscount { get; set; }
    }
}