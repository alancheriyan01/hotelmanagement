﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project_work.DataService;
using project_work.Entity;

namespace project_work.Presentation
{
    public partial class PaymentType : System.Web.UI.Page
    {
        public static int id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !IsCallback)
            {
                if (Request.QueryString["id"] != null)
                {
                    id = Convert.ToInt32(Request.QueryString["id"].ToString().Trim());
                    GetDataById(id);
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    btnDelete.Visible = true;
                    heading.InnerText = "Edit PaymentType ";
                }
                else
                {
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    heading.InnerText = "Create New PaymentType";
                }
            }
        }

        private void GetDataById(int id)
        {
            PaymentTypeService objservice = new PaymentTypeService();
            DataSet _fetchData = objservice.GetDataByid(id);
            if (_fetchData.Tables[0].Rows.Count > 0)
            {
                txtPaymentType.Text = _fetchData.Tables[0].Rows[0]["paymentType"].ToString();
                txtDescription.Text = _fetchData.Tables[0].Rows[0]["description"].ToString();
               
            }
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            string myStringVariable = string.Empty;
            PaymentTypeEntity customerentity = new PaymentTypeEntity
            {
                PaymentType = txtPaymentType.Text,
                Description=txtDescription.Text
            };
            PaymentTypeService ObjService = new PaymentTypeService();
            int rowsAffected = ObjService.InsertData(customerentity);
            if (rowsAffected == 1)
            {
                myStringVariable = "Saved Successfully";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
            }
            else
            {
                myStringVariable = "Operation Denied";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }

        protected void Btnupdate_Click(object sender, EventArgs e)
        {
            string myStringVariable = string.Empty;
            PaymentTypeEntity entity = new PaymentTypeEntity
            {
                PaymentTypeID = id,
                PaymentType = txtPaymentType.Text,
                Description = txtDescription.Text,
              
            };
            PaymentTypeService ObjService = new PaymentTypeService();
            int rowsAffected = ObjService.UpdateData(entity);
            if (rowsAffected == 1)
            {
                myStringVariable = "Updated Successfully";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
            }
            else
            {
                myStringVariable = "Operation Denied";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }
        protected void Btndelete_Click(object sender, EventArgs e)
        {

            string myStringVariable = string.Empty;
            PaymentTypeEntity entity = new PaymentTypeEntity
            {
                PaymentTypeID = id

            };
            PaymentTypeService ObjService = new PaymentTypeService();
            int rowsAffected = ObjService.DeleteData(entity);
            if (rowsAffected == 1)
            {
                myStringVariable = "Deleted Successfully";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                Response.Redirect("./PaymentTypeList.aspx");
            }
            else
            {
                myStringVariable = "Operation Denied";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("./PaymentTypeList.aspx");
        }

    }
}