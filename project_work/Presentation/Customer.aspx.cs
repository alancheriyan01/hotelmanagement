﻿using project_work.DataService;
using project_work.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project_work.Presentation
{
   
    public partial class Customer : System.Web.UI.Page
    {
        public static int id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !IsCallback)
            {
                if(Request.QueryString["id"]!=null)
                {
                    id =Convert.ToInt32( Request.QueryString["id"].ToString().Trim());
                    GetDataById(id);
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    btnDelete.Visible = true;
                    heading.InnerText = "Edit Customer Data";
                }
                else
                {
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    heading.InnerText = "Create New Customer Data";
                }
            }
        }

        private void GetDataById(int id)
        {
            CustomerDataService objservice = new CustomerDataService();
            DataSet _fetchData = objservice.GetDataByid(id);
            if (_fetchData.Tables[0].Rows.Count > 0)
            {
                txtname.Text = _fetchData.Tables[0].Rows[0]["customer_name"].ToString();
                txtcompany.Text = _fetchData.Tables[0].Rows[0]["company"].ToString();
                txtemail.Text = _fetchData.Tables[0].Rows[0]["customer_email"].ToString();
                txtdiscount.Text = _fetchData.Tables[0].Rows[0]["discount"].ToString();
            }
        }

        static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            string myStringVariable = string.Empty;
            
            //string key = "this_is_a_key@123";
            string hashedpassword = ComputeSha256Hash(txtPassword.Text);
            //byte[] data = UTF8Encoding.UTF8.GetBytes(txtPassword.Text);
            //using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            //{
            //    byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            //    using (TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
            //    {
            //        ICryptoTransform transform = tripleDes.CreateEncryptor();
            //        byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
            //        hashedpassword = Convert.ToBase64String(results);
            //    }
            //}

            //byte[] data1 = Convert.FromBase64String(txtname.Text);
            //using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            //{
            //    byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            //    using (TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
            //    {
            //        ICryptoTransform transform = tripleDes.CreateDecryptor();
            //        byte[] results = transform.TransformFinalBlock(data1, 0, data1.Length);
            //        Label1.Text = UTF8Encoding.UTF8.GetString(results);
            //    }
            //}
            CustomerEntity customerentity = new CustomerEntity
            {
                CustomerName = txtname.Text,
                CustomerCompany = txtcompany.Text,
                CustomerDiscount = txtdiscount.Text,
                CustomerEmail = txtemail.Text,
                Password=hashedpassword
            };
            CustomerDataService ObjService = new CustomerDataService();
            int rowsAffected = 0;
            rowsAffected=ObjService.InsertData(customerentity);
            if (rowsAffected == 1)
            {
                myStringVariable = "Saved Successfully";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
            }
            else
            {
                myStringVariable = "Operation Denied";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }

        protected void Btnupdate_Click(object sender, EventArgs e)
        {
            string myStringVariable = string.Empty;
            CustomerEntity customerentity = new CustomerEntity
            {
                CustomerID = id,
                CustomerName = txtname.Text,
                CustomerCompany = txtcompany.Text,
                CustomerDiscount = txtdiscount.Text,
                CustomerEmail = txtemail.Text
            };
            CustomerDataService ObjService = new CustomerDataService();
            int rowsAffected = ObjService.UpdateData(customerentity);
            if (rowsAffected == 1)
            {
                myStringVariable = "Updated Successfully";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
            }
            else
            {
                myStringVariable = "Operation Denied";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }
        protected void Btndelete_Click(object sender, EventArgs e)
        {

            string myStringVariable = string.Empty;
            CustomerEntity customerentity = new CustomerEntity
            {
                CustomerID = id
                
            };
            CustomerDataService ObjService = new CustomerDataService();
            int rowsAffected = ObjService.DeleteData(customerentity);
            if (rowsAffected == 1)
            {
                myStringVariable = "Deleted Successfully";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                Response.Redirect("./CustomerList.aspx");
            }
            else
            {
                myStringVariable = "Operation Denied";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("./CustomerList.aspx");
        }
        
    }
}