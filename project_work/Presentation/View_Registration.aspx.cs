﻿using project_work.DataService;
using project_work.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project_work.Presentation
{
    public partial class View_Registration : System.Web.UI.Page
    {
        public int UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetDataByID();
            }
        }

        public void GetDataByID()
        {
            UserId = Convert.ToInt32(Session["UserID"]);
            RegistrationDataService objservice = new RegistrationDataService();
            DataSet _fetchData = objservice.GetDataCustomerByid(UserId);

            if (_fetchData.Tables[0].Rows.Count > 0)
            {
                lblBkId.Text = _fetchData.Tables[0].Rows[0]["Reservation_Id"].ToString();

                lblArrivalDate.Text = _fetchData.Tables[0].Rows[0]["Arrival_Date"].ToString();
                lblStatus.Text = _fetchData.Tables[0].Rows[0]["status"].ToString();
                lblRoom.Text= _fetchData.Tables[0].Rows[0]["RoomNumber"].ToString();

            }
            else
            {
                lblArrivalDate1.Visible = false;
                lblArrivalDate2.Visible = false;
                lblArrivalDate.Visible = false;
                lblBkId.Visible = false;
                lblBkId2.Visible = false;
                // lblBkId1.Visible = false;
                lblStatus.Visible = false;
                lblStatus1.Visible = false;
                lblStatus2.Visible = false;
                lblRoom1.Visible = false;
                lblRoom2.Visible = false;
                lblBkId1.Text = "No Record Found";
                btnEmail.Visible = false;

            }

        }

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                string fromaddr = "forprojectfkg@gmail.com";
                string toaddr = "alan6009@gmail.com";//TO ADDRESS HERE
                string password = "omkv@123";

                MailMessage msg = new MailMessage();
                msg.Subject = "Booking confirmation";
                msg.From = new MailAddress(fromaddr);
                msg.Body = "Booding Id:"+lblBkId.Text+"\nRoom Number:"+lblRoom.Text+"\nArrival Date:"+lblArrivalDate.Text+"\nStatus:"+lblStatus.Text;
                msg.To.Add(new MailAddress(toaddr));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.EnableSsl = true;
                NetworkCredential nc = new NetworkCredential(fromaddr, password);
                smtp.Credentials = nc;
                smtp.Send(msg);

                string myStringVariable = "Mail Send Successfully";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);


            }
            catch (Exception ex)
            {
                string myStringVariable = "Mail Send Failed";

                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
            }

        }
    }
}