﻿using project_work.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using project_work.DataService;
using System.Text;
using System.Security.Cryptography;

namespace project_work.Presentation
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

            //string key = "this_is_a_key@123";
            string hashedpassword =ComputeSha256Hash(txtPassword.Text);
           
            LoginEntity Entity = new LoginEntity
            {
                Username = txtUsername.Text,
                Password = hashedpassword
            };
            LoginDataService Service = new LoginDataService();
            int Id = Service.GetDataByid(Entity);
            if (Id != 0)
            {
                Session["UserID"] = Id;
                Response.Redirect("./View_Registration.aspx");
            }
            else
            {
                string myStringVariable = "Operation Denied";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);



            }
        }
    }
}