﻿using project_work.DataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project_work.Presentation
{
    public partial class PaymentTypeList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                gvbind();
            }
        }

        private void gvbind()
        {

            PaymentTypeService OBJService = new PaymentTypeService();
            System.Data.DataSet ds_fetchData = OBJService.FetchData();
            if (ds_fetchData.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds_fetchData;
                GridView1.DataBind();
            }
            else
            {
                ds_fetchData.Tables[0].Rows.Add(ds_fetchData.Tables[0].NewRow());
                GridView1.DataSource = ds_fetchData;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            // GridView1.EditIndex = e.NewEditIndex;
            int id = Convert.ToInt32(GridView1.DataKeys[e.NewEditIndex].Value.ToString());
            Response.Redirect("./PaymentType.aspx?id=" + id);
            //  gvbind();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            gvbind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("./PaymentType.aspx");
        }
    }
}