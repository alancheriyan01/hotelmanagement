﻿using project_work.DataService;
using project_work.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project_work.Presentation
{
    public partial class Invoice : System.Web.UI.Page
    {
       
   public static int id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !IsCallback)
            {
                FetchPaymentType();
                if (Request.QueryString["id"] != null)
                {
                    id = Convert.ToInt32(Request.QueryString["id"].ToString().Trim());
                    GetDataById(id);
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    btnDelete.Visible = true;
                    heading.InnerText = "Edit Invoice Data";
                }
                else
                {
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    heading.InnerText = "Create New Invoice Data";
                }
            }
        }

        private void GetDataById(int id)
        {
            InvoiceService objservice = new InvoiceService();
            DataSet _fetchData = objservice.GetDataByid(id);
            FetchPaymentType();
            if (_fetchData.Tables[0].Rows.Count > 0)
            {
                txtReservation.Text = _fetchData.Tables[0].Rows[0]["Reservation_Id"].ToString();
                ddlpaymentType.SelectedValue = _fetchData.Tables[0].Rows[0]["Payment_Id"].ToString();
                txtstatus.Text = _fetchData.Tables[0].Rows[0]["Status"].ToString();
                txttotalamount.Text = _fetchData.Tables[0].Rows[0]["Total_amount"].ToString();
                Calendar1.SelectedDate=Convert.ToDateTime( _fetchData.Tables[0].Rows[0]["Payment_Date"].ToString());
            }
        }

        private void FetchPaymentType()
        {
            InvoiceService objservice = new InvoiceService();
            DataSet _fetchData = objservice.FetchPaymentType();

            if (_fetchData.Tables[0].Rows.Count > 0)
            {
                ddlpaymentType.DataSource = _fetchData.Tables[0];
                ddlpaymentType.DataBind();
                ddlpaymentType.DataValueField = "PaymentID";
                ddlpaymentType.DataTextField = "PaymentType";
                ddlpaymentType.DataBind();
                ddlpaymentType.SelectedIndex = 0;

            }
        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            string myStringVariable = string.Empty;
            InvoiceEntity InvoiceEntity = new InvoiceEntity
            {
                Reservation_Id =Convert.ToInt32( txtReservation.Text),
                Payment_Id =Convert.ToInt32( ddlpaymentType.SelectedItem.Value),
                Payment_Date =Convert.ToString( Calendar1.SelectedDate),
                Total_amount =Convert.ToDouble( txttotalamount.Text),
                Status=txtstatus.Text
            };
            InvoiceService ObjService = new InvoiceService();
            int rowsAffected = ObjService.InsertData(InvoiceEntity);
            if (rowsAffected == 1)
            {
                myStringVariable = "Saved Successfully";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
            }
            else
            {
                myStringVariable = "Operation Denied";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }

        protected void Btnupdate_Click(object sender, EventArgs e)
        {
            string myStringVariable = string.Empty;
            InvoiceEntity InvoiceEntity = new InvoiceEntity
            {
                Invoice_number = id,
                Reservation_Id = Convert.ToInt32(txtReservation.Text),
                Payment_Id = Convert.ToInt32(ddlpaymentType.SelectedItem.Value),
                Payment_Date = Convert.ToString(Calendar1.SelectedDate),
                Total_amount = Convert.ToDouble(txttotalamount.Text),
                Status = txtstatus.Text
            };
            InvoiceService ObjService = new InvoiceService();
            int rowsAffected = ObjService.UpdateData(InvoiceEntity);
            if (rowsAffected == 1)
            {
                myStringVariable = "Updated Successfully";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
            }
            else
            {
                myStringVariable = "Operation Denied";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }
        protected void Btndelete_Click(object sender, EventArgs e)
        {

            string myStringVariable = string.Empty;
            InvoiceEntity InvoiceEntity = new InvoiceEntity
            {
                Invoice_number = id

            };
            InvoiceService ObjService = new InvoiceService();
            int rowsAffected = ObjService.DeleteData(InvoiceEntity);
            if (rowsAffected == 1)
            {
                myStringVariable = "Deleted Successfully";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                // Response.Redirect("./InvoiceList.aspx");
                btnUpdate.Visible = false;
                Response.Redirect("./InvoiceList.aspx");
            }
            else
            {
                myStringVariable = "Operation Denied";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("./InvoiceList.aspx");
        }

    
}
}