﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoiceList.aspx.cs" Inherits="project_work.Presentation.InvoiceList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        .button {
            display: inline-block;
            border-radius: 4px;
            background-color: #5683e8;
            border: none;
            color: #FFFFFF;
            text-align: center;
            font-size: 14px;
            padding: 10px;
            width: 100px;
            transition: all 0.5s;
            cursor: pointer;
            margin: 5px;
        }

            .button span {
                cursor: pointer;
                display: inline-block;
                position: relative;
                transition: 0.5s;
            }

                .button span:after {
                    content: '\00bb';
                    position: absolute;
                    opacity: 0;
                    top: 0;
                    right: -20px;
                    transition: 0.5s;
                }

            .button:hover span {
                padding-right: 25px;
            }

                .button:hover span:after {
                    opacity: 1;
                    right: 0;
                }
    </style>

    <title></title>
    <link href="../StyleSheet/newCustomer.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin-left: 60%">
            <asp:Button ID="Button1" class="button" Style="vertical-align: middle" runat="server" Text="Add New" OnClick="Button1_Click" />
        </div>
        <div class="gridstyle" style="padding-left: 100px; padding-top: 10px;">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false"
                DataKeyNames="Invoice_number"
                OnRowCancelingEdit="GridView1_RowCancelingEdit"
                OnRowEditing="GridView1_RowEditing">
                <Columns>
                    <asp:BoundField DataField="Invoice_number" HeaderText="Invoice number" ReadOnly="true" />
                    <asp:BoundField DataField="Reservation_Id" HeaderText="Registration ID" />
                    
                    <asp:BoundField DataField="Payment_Date" HeaderText="Payment Date" />
                    <asp:BoundField DataField="Total_amount" HeaderText="Total amount" />
                    <asp:BoundField DataField="Status" HeaderText="Status" />

                    <asp:CommandField ShowEditButton="true" />
                </Columns>
            </asp:GridView>
        </div>

    </form>
</body>
</html>
