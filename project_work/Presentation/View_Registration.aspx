﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="View_Registration.aspx.cs" Inherits="project_work.Presentation.View_Registration" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <style type="text/css">
        .button {
            display: inline-block;
            border-radius: 4px;
            background-color: #5683e8;
            border: none;
            color: #FFFFFF;
            text-align: center;
            font-size: 14px;
            padding: 10px;
            width: 100px;
            transition: all 0.5s;
            cursor: pointer;
            margin: 5px;
        }

            .button span {
                cursor: pointer;
                display: inline-block;
                position: relative;
                transition: 0.5s;
            }

                .button span:after {
                    content: '\00bb';
                    position: absolute;
                    opacity: 0;
                    top: 0;
                    right: -20px;
                    transition: 0.5s;
                }

            .button:hover span {
                padding-right: 25px;
            }

                .button:hover span:after {
                    opacity: 1;
                    right: 0;
                }
    </style>

    <div class="jumbotron">
        <h3>Booking Details</h3>
       
     <table>
            <tr>
                    <td>
                        <asp:Label ID="lblBkId1" runat="server" Text="Booking Id"></asp:Label>

                    </td>
                 <td>
                        <asp:Label ID="lblBkId2" runat="server" Text=":"></asp:Label>

                    </td>
                    <td>
                      <asp:Label ID="lblBkId" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
          <tr>
                    <td>
                        <asp:Label ID="lblArrivalDate1" runat="server" Text="Arrival Date"></asp:Label>

                    </td>
              <td>
                        <asp:Label ID="lblArrivalDate2" runat="server" Text=":"></asp:Label>

                    </td>
                    <td>
                      <asp:Label ID="lblArrivalDate" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
           <tr>
                    <td>
                        <asp:Label ID="lblRoom1" runat="server" Text="Room Number"></asp:Label>

                    </td>
              <td>
                        <asp:Label ID="lblRoom2" runat="server" Text=":"></asp:Label>

                    </td>
                    <td>
                      <asp:Label ID="lblRoom" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
          <tr>
                    <td>
                        <asp:Label ID="lblStatus1" runat="server" Text="Status"></asp:Label>

                    </td>
              <td>
                        <asp:Label ID="lblStatus2" runat="server" Text=":"></asp:Label>

                    </td>
                    <td >
                      <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
             
          <tr>
                    
                    <td colspan="2">
                        <asp:Button ID="btnEmail" class="button" runat="server" Text="Send Email" style="margin-left:100px;vertical-align: middle" OnClick="btnEmail_Click"  />
                        
                    </td>
                </tr>
     </table>
    </div>

  
</asp:Content>
