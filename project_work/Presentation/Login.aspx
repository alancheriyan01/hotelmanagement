﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Login.aspx.cs" Inherits="project_work.Presentation.Login" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

  <link href="../StyleSheet/loginsheet.css" rel="stylesheet" type="text/css" />

    <div class="jumbotron">
          <p><img src="../img/logo.png"  alt="logo"  class="image"/></p>
        <h3>Login</h3>
      
       
     <table>

            <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Username"></asp:Label>

                    </td>
                    <td>
                      <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
                    </td>
                </tr>
          <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Password"></asp:Label>

                    </td>
                    <td style="padding-top:5px">
                       <asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
                    </td>
                </tr>
             
          <tr>
                    
                    <td colspan="2">
                        <asp:Button ID="btnLogin" class="button" runat="server" Text="Login" style="margin-left:100px;vertical-align: middle" OnClick="btnLogin_Click" />
                        
                    </td>
                </tr>
     </table>
    </div>

  
</asp:Content>
