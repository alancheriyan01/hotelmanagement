﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Customer.aspx.cs" Inherits="project_work.Presentation.Customer" %>

<!DOCTYPE html>
<link href="../StyleSheet/newCustomer.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        .button {
            display: inline-block;
            border-radius: 4px;
            background-color: #5683e8;
            border: none;
            color: #FFFFFF;
            text-align: center;
            font-size: 14px;
            padding: 10px;
            width: 100px;
            transition: all 0.5s;
            cursor: pointer;
            margin: 5px;
        }

            .button span {
                cursor: pointer;
                display: inline-block;
                position: relative;
                transition: 0.5s;
            }

                .button span:after {
                    content: '\00bb';
                    position: absolute;
                    opacity: 0;
                    top: 0;
                    right: -20px;
                    transition: 0.5s;
                }

            .button:hover span {
                padding-right: 25px;
            }

                .button:hover span:after {
                    opacity: 1;
                    right: 0;
                }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="divStyle" style="padding-left:20px">
            <h4 id="heading" runat="server">Create New Customer</h4>
            <table border="0">
                <tr>
                    <td>
                        <asp:Label ID="lbname" runat="server" Text="Name"></asp:Label>

                    </td>
                    <td>
                        <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Email"></asp:Label>

                    </td>
                    <td>
                        <asp:TextBox ID="txtemail" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Password"></asp:Label>

                    </td>
                    <td>
                       <asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Comapny"></asp:Label>

                    </td>
                    <td>
                      <asp:TextBox ID="txtcompany" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Discount"></asp:Label>

                    </td>
                    <td>
                       <asp:TextBox ID="txtdiscount" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    
                    <td colspan="2">
                        <asp:Button ID="btnSave" class="button" runat="server" Text="Save" style="margin-left:100px;vertical-align: middle" OnClick="BtnSave_Click"/>
                         <asp:Button ID="btnUpdate" class="button" runat="server" Text="Update" style="margin-left:100px;vertical-align: middle" OnClick="Btnupdate_Click"/>
                        <asp:Button ID="btnDelete" class="button" runat="server" Text="Delete" style="margin-left:100px;vertical-align: middle" OnClick="Btndelete_Click"/>
                         <asp:Button ID="btnBack"  class="button" runat="server" Text="Back" style="margin-left:100px;vertical-align: middle" OnClick="BtnBack_Click"/>
                    </td>
                </tr>
            </table>

        </div>

    </form>
</body>
</html>
