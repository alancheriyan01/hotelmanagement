﻿using project_work.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace project_work.DataService
{
    public class InvoiceService
    {

        public DataSet FetchData()
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            DataSet _dataSet = new DataSet();
            try
            {
                conn = new SqlConnection(ConnectionString);
                string commandString = "SELECT * from [Invoice];";

                SqlDataAdapter _dataAdapter = new SqlDataAdapter(commandString, conn);

                _dataAdapter.Fill(_dataSet, "Payment");

                return _dataSet;

            }
            catch (Exception ex)
            {
                conn.Close();
            }
            return _dataSet;
        }
        public DataSet GetDataByid(int id)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            DataSet _dataSet = new DataSet();
            try
            {
                conn = new SqlConnection(ConnectionString);
                string commandString = "SELECT * from [Invoice] where [Invoice_number]=" + id + ";";

                SqlDataAdapter _dataAdapter = new SqlDataAdapter(commandString, conn);

                _dataAdapter.Fill(_dataSet, "Payment");

                return _dataSet;
            }
            catch (Exception ex)
            {
                conn.Close();
            }
            return _dataSet;
        }
        public int InsertData(InvoiceEntity entity)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    int maxid = GetId();
                    cmd.CommandText = "INSERT INTO [dbo].[Invoice]([Invoice_number],[Reservation_Id],[Payment_Date],[Payment_Id],[Total_amount],[Status] " +
                        ") Values (@var,@var1,@var2,@var3,@var4,@var5)";
                    cmd.Parameters.AddWithValue("@var", maxid + 1);
                    cmd.Parameters.AddWithValue("@var1", entity.Reservation_Id);
                    cmd.Parameters.AddWithValue("@var2", entity.Payment_Date);
                    cmd.Parameters.AddWithValue("@var3", entity.Payment_Id);
                    cmd.Parameters.AddWithValue("@var4", entity.Total_amount);
                    cmd.Parameters.AddWithValue("@var5", entity.Status);
                    rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {

                conn.Close();
                return 0;
            }
            finally
            {
                conn.Close();

            }

        }

        public DataSet FetchPaymentType()
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            DataSet _dataSet = new DataSet();
            try
            {
                conn = new SqlConnection(ConnectionString);
                string commandString = "SELECT * from [Payment];";

                SqlDataAdapter _dataAdapter = new SqlDataAdapter(commandString, conn);

                _dataAdapter.Fill(_dataSet, "Payment");

                return _dataSet;

            }
            catch (Exception ex)
            {
                conn.Close();
            }
            return _dataSet;
        }

        public int UpdateData(InvoiceEntity entity)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "UPDATE [dbo].[Invoice] SET [Reservation_Id] = " + entity.Reservation_Id + ",[Payment_Date] = '"+entity.Payment_Date+"',[Payment_Id] = "+entity.Payment_Id+",[Total_amount] = "+entity.Total_amount+",[Status] = '"+entity.Status+ "' WHERE [Invoice_number] ="+entity.Invoice_number+";";

                    rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {

                conn.Close();
                return 0;
            }
            finally
            {
                conn.Close();

            }

        }

        public int DeleteData(InvoiceEntity entity)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "DELETE FROM [dbo].[Invoice] WHERE [Invoice_number]=" + entity.Invoice_number;

                    rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {

                conn.Close();
                return 0;
            }
            finally
            {
                conn.Close();

            }

        }


        public int GetId()
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                string commandString = "SELECT max(Invoice_number) id from [Invoice];";

                SqlDataAdapter _dataAdapter = new SqlDataAdapter(commandString, conn);
                DataSet _dataSet = new DataSet();
                _dataAdapter.Fill(_dataSet, "payment");

                if (_dataSet.Tables[0].Rows.Count > 0)
                {
                    return Convert.ToInt32(_dataSet.Tables[0].Rows[0]["id"]);
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {
                conn.Close();
            }
            return 0;
        }
    }
}