﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using project_work.Entity;
using System.Data;

namespace project_work.DataService
{
    public class CustomerDataService
    {

        public DataSet FetchData()
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            DataSet _dataSet = new DataSet();
            try
            {
                conn = new SqlConnection(ConnectionString);
                string commandString = "SELECT * from Customer;";

                SqlDataAdapter _dataAdapter = new SqlDataAdapter(commandString, conn);

                _dataAdapter.Fill(_dataSet, "Payment");

                return _dataSet;

            }
            catch (Exception ex)
            {
                conn.Close();
            }
            return _dataSet;
        }
        public DataSet GetDataByid(int id)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            DataSet _dataSet = new DataSet();
            try
            {
                conn = new SqlConnection(ConnectionString);
                string commandString = "SELECT * from Customer where customer_id=" + id + ";";

                SqlDataAdapter _dataAdapter = new SqlDataAdapter(commandString, conn);

                _dataAdapter.Fill(_dataSet, "Customers");

                return _dataSet;
            }
            catch (Exception ex)
            {
                conn.Close();
            }
            return _dataSet;
        }
        public int InsertData(CustomerEntity customerentity)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            int rowsAffected = 0;                                                                                                                                                          
            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    int maxCustomerId = GetCustomerID();
                    cmd.CommandText = "INSERT INTO Customer(Customer_id,Customer_name,customer_email,company,discount" +
                        ") Values (@var,@var1,@var2,@var3,@var4)";
                    cmd.Parameters.AddWithValue("@var", maxCustomerId + 1);
                    cmd.Parameters.AddWithValue("@var1", customerentity.CustomerName);
                    cmd.Parameters.AddWithValue("@var2", customerentity.CustomerEmail);
                    cmd.Parameters.AddWithValue("@var3", customerentity.CustomerCompany);
                    cmd.Parameters.AddWithValue("@var4", customerentity.CustomerDiscount);
                    rowsAffected = cmd.ExecuteNonQuery();
                    rowsAffected = CreateLogin(customerentity, maxCustomerId + 1);
                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {

                conn.Close();
                return 0;
            }
            finally
            {
                conn.Close();

            }

        }

        public int CreateLogin(CustomerEntity Entity,int Cust_id)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                   
                    cmd.CommandText = "INSERT INTO Login(Customer_id,Customer_username,customer_password" +
                        ") Values (@var,@var1,@var2)";
                    cmd.Parameters.AddWithValue("@var", Cust_id);
                    
                    cmd.Parameters.AddWithValue("@var1", Entity.CustomerEmail);
                    cmd.Parameters.AddWithValue("@var2", Entity.Password);
                   
                    rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {

                conn.Close();
                return 0;
            }
            finally
            {
                conn.Close();

            }
        }

        public int UpdateData(CustomerEntity customerentity)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "UPDATE [dbo].[Customer] SET [Customer_name] = '" + customerentity.CustomerName + "',[customer_email] = '" + customerentity.CustomerEmail + "',[company] = '" + customerentity.CustomerCompany + "',[discount] = " + customerentity.CustomerDiscount + " WHERE [Customer_id]=" + customerentity.CustomerID;

                    rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {

                conn.Close();
                return 0;
            }
            finally
            {
                conn.Close();

            }

        }

        public int DeleteData(CustomerEntity customerentity)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "DELETE FROM [dbo].[Customer] WHERE [Customer_id]=" + customerentity.CustomerID;

                    rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {

                conn.Close();
                return 0;
            }
            finally
            {
                conn.Close();

            }

        }


        public int GetCustomerID()
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                string commandString = "SELECT max(Customer_id) id from Customer;";

                SqlDataAdapter _dataAdapter = new SqlDataAdapter(commandString, conn);
                DataSet _dataSet = new DataSet();
                _dataAdapter.Fill(_dataSet, "Customers");

                if (_dataSet.Tables[0].Rows.Count > 0)
                {
                    return Convert.ToInt32(_dataSet.Tables[0].Rows[0]["id"]);
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {
                conn.Close();
            }
            return 0;
        }
    }


}