﻿using project_work.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace project_work.DataService
{
    public class LoginDataService
    {
        public int GetDataByid(LoginEntity Entity)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                   
                    cmd.CommandText = "SELECT * FROM Login WHERE Customer_username = @var and customer_password= @var1 ";
                    cmd.Parameters.Add("@var", SqlDbType.VarChar);
                    cmd.Parameters["@var"].Value = Entity.Username;
                    cmd.Parameters.Add("@var1", SqlDbType.VarChar);
                    cmd.Parameters["@var1"].Value = Entity.Password;
                    //cmd.Parameters.AddWithValue("@var", Entity.Username);
                    //cmd.Parameters.AddWithValue("@var1",Entity.Password);

                    SqlDataReader reader  = cmd.ExecuteReader();
                    int id=0;
                    while (reader.Read())
                    {
                         id = reader.GetInt32(0);
                    }
                   
                    return id;
                }
            }
            catch (Exception ex)
            {

                conn.Close();
                return 0;
            }
            finally
            {
                conn.Close();

            }
        }
    }
}