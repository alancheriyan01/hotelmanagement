﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using project_work.Entity;
using System.Data;

namespace project_work.DataService
{
    public class RegistrationDataService
    {

        public DataSet GetDataCustomerByid(int id)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            DataSet _dataSet = new DataSet();
            try
            {
                conn = new SqlConnection(ConnectionString);
                string commandString = "SELECT [Reservation_Id],[Customer_id],[RoomNumber],[Arrival_Date],[Departure_Date],[Customized_Price],[status] from Reservation  rs inner join [Room] rd on rs.Room_id=rd.RoomId and rs.Customer_id=" + id + ";";

                SqlDataAdapter _dataAdapter = new SqlDataAdapter(commandString, conn);

                _dataAdapter.Fill(_dataSet, "Registration");

                return _dataSet;
            }
            catch (Exception ex)
            {
                conn.Close();
            }
            return _dataSet;
        }

    }
}