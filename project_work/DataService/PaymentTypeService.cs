﻿using project_work.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace project_work.DataService
{
    public class PaymentTypeService
    {

        public DataSet FetchData()
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            DataSet _dataSet = new DataSet();
            try
            {
                conn = new SqlConnection(ConnectionString);
                string commandString = "SELECT * from [Payment];";

                SqlDataAdapter _dataAdapter = new SqlDataAdapter(commandString, conn);

                _dataAdapter.Fill(_dataSet, "Payment");

                return _dataSet;

            }
            catch (Exception ex)
            {
                conn.Close();
            }
            return _dataSet;
        }
        public DataSet GetDataByid(int id)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            DataSet _dataSet = new DataSet();
            try
            {
                conn = new SqlConnection(ConnectionString);
                string commandString = "SELECT * from [Payment] where [PaymentID]=" + id + ";";

                SqlDataAdapter _dataAdapter = new SqlDataAdapter(commandString, conn);

                _dataAdapter.Fill(_dataSet, "Payment");

                return _dataSet;
            }
            catch (Exception ex)
            {
                conn.Close();
            }
            return _dataSet;
        }
        public int InsertData(PaymentTypeEntity entity)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    int maxCustomerId = GetId();
                    cmd.CommandText = "INSERT INTO [dbo].[Payment](PaymentID,[PaymentType],[Description] "+
                        ") Values (@var,@var1,@var2)";
                    cmd.Parameters.AddWithValue("@var", maxCustomerId + 1);
                    cmd.Parameters.AddWithValue("@var1", entity.PaymentType);
                    cmd.Parameters.AddWithValue("@var2", entity.Description);
                    
                    rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {

                conn.Close();
                return 0;
            }
            finally
            {
                conn.Close();

            }

        }

        public int UpdateData(PaymentTypeEntity entity)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "UPDATE [dbo].[Payment] SET [PaymentType] = '" + entity.PaymentType + "',[Description] = '" + entity.Description + "' WHERE PaymentID=" + entity.PaymentTypeID;

                    rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {

                conn.Close();
                return 0;
            }
            finally
            {
                conn.Close();

            }

        }

        public int DeleteData(PaymentTypeEntity entity)
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "DELETE FROM [dbo].[Payment] WHERE [PaymentID]=" + entity.PaymentTypeID;

                    rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {

                conn.Close();
                return 0;
            }
            finally
            {
                conn.Close();

            }

        }


        public int GetId()
        {
            Stringconnection OBJConnectionString = new Stringconnection();
            string ConnectionString = OBJConnectionString.Getconnection();
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                string commandString = "SELECT max(PaymentID) id from Payment;";

                SqlDataAdapter _dataAdapter = new SqlDataAdapter(commandString, conn);
                DataSet _dataSet = new DataSet();
                _dataAdapter.Fill(_dataSet, "payment");

                if (_dataSet.Tables[0].Rows.Count > 0)
                {
                    return Convert.ToInt32(_dataSet.Tables[0].Rows[0]["id"]);
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {
                conn.Close();
            }
            return 0;
        }
    }
}